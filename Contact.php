<?php

# Import the layout and menu
include("components/interfaceSettings.php");
include("components/menu.php");

PrintHeader("ASPASIA: Contact Details");

# Javascript method to check the data entry on the form
print '</head>';


#### Now set up the HTML Page

print '
<br>
<div id="textheading">ASPASIA: Contact Details</div>
<br>
<div id="formtable">
<table width="85%"  border="0" cellspacing="0" cellpadding="0">
<TR><TD>ASPASIA has been developed by researchers in the <a href="http://www.york.ac.uk/ycil">York Computational Immunology Lab</a> at the <a href="http://www.york.ac.uk">University of York</a>.<br><br>
The tool itself was developed by Stephanie Dyson, <a href="http://www.york.ac.uk/electronics/staff/kieran_alden">Kieran Alden</a>, and <a href="http://www.elec.york.ac.uk/staff/jt517.html">Jon Timmis</a>.<br><br>We are grateful to <a href="https://www.york.ac.uk/cii/staff/academic/kullberg/">Marika Kullberg</a> and <a href="https://www.york.ac.uk/cii/staff/academic/coles/">Mark Coles</a> at the University of York, and Lourdes Cucurull-Sanchez and Christopher Larmine at GSK, for providing biological expertise in model construction and understanding generated results.
<br><br> The study in which ASPASIA was developed was supported by a BBSRC CASE award in conjunction with GlaxoSmithKline. Salary support for Kieran Alden was provided through the Centre for Chronic Diseases and Disorders (C2D2) at the University of York, funded by the Wellcome Trust. Jon Timmis is part funded by The Royal Society and The Royal Academy of Engineering. </TD></TR></TABLE>
</div>
<BR>';

?>
