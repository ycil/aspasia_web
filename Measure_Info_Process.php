<?php

# Session start must be in all pages that write / access session variables
session_start();

#### Now set up the HTML Page

if(isset($_POST['addAnother']))
{
	# Get the parameter name
	array_push($_SESSION["measureNames"],$_POST['measureName']);

	# Then do per the analysis
	if($_SESSION['experimentType']=="Robustness" || $_SESSION['experimentType']=="LHC" || $_SESSION['experimentType']=="eFAST")
	{
		array_push($_SESSION["measureScales"],$_POST['scale']);		
	}
	else if($_SESSION['experimentType']=="NewEvent")
	{
		# To Do
	}

	# Redirect back so more parameters can be added
	header('Location: Measure_Info.php') ;

}
else
{
	# If Robustness, we can skip this page altogether
	if($_SESSION['experimentType']=="Robustness")
	{
		header( 'Location: GenerateSettingsFile.php' );
	}
	else
	{
		# Redirect to the next pages of the interface form
		header( 'Location: Analysis_Specific_Parameters.php' );
	}
}

?>
