<?php

# Import the layout and menu
include("components/interfaceSettings.php");
include("components/menu.php");

PrintHeader("ASPASIA: Publication");

# Javascript method to check the data entry on the form
print '</head>';


#### Now set up the HTML Page

print '
<br>
<div id="textheading">ASPASIA: Case Study and Details for Worked Example</div>
<br>
<div id="formtable">
<table width="85%"  border="0" cellspacing="0" cellpadding="0">
<TR><TD>CD4 + T helper cells play a crucial role in the adaptive immune response following their activation through recognition of peptide/MHC complexes on antigen-presenting cells. In the presence of specific cyokines, activated CD4 + T cells differentiate into distinct subsets that are characterised by their cytokine-secretion profile and their expression of specific transcription factors. Thus, IL-12 drives the differentiation of Th1 cells that secrete IFN-γ and express T-bet, IL-4 drives the differentiation of Th2 cells that secrete IL-4 and express GATA-3, and a combination of IL-6 and TGF-β, along with IL-21 and IL-23 are responsible for the differentiation, maintenance and expansion of Th17 cells that secrete IL-17, IL-21, and express RORγt. Th17 cells have, under certain conditions, been shown to switch phenotype from an IL-17-producing Th17 cell to an IFN-γ-producing ex-Th17 cell that expresses T-bet and not RORγt (see Figure below). While in vitro-derived Th17 cells express a functional IL-12 receptor and turn on IFN-γ production following IL-12 stimulation, ex vivo-derived Th17 cells lack a functional IL-12 receptor, suggesting that factors other than IL-12 are responsible for phenotype switching in vivo.<br><br>
We have developed an SBML model to capture in silico the dynamics of Th17-cell plasticity in vivo (see part B of Figure below). This model can be used to infer the dynamics of a hypothetical receptor and cytokine involved in the phenotype switching of Th17 cells. The model builds on work by <a href="http://www.ncbi.nlm.nih.gov/pubmed/15380383">Yates et al</a> and <a href="http://www.ncbi.nlm.nih.gov/pubmed/19409816">Schulz et al</a> to capture the dynamics of transcription factors T-bet and RORγt in a CD4 + T cell undergoing polarisation and phenotype switching following exposure to exogenous cytokines.<br><br>

To recreate the analyses shown in the ASPASIA publication, we provide:
<ul>
<li><a href="resources/Steps.pdf">List of Steps taken to produce the described results</a></li>
<li><a href="resources/Polarisation_Model.xml">The SBML polarisation model we have developed and analysed using ASPASIA (right-click, save link as)</a></li>
</ul>

</TD></TR></TABLE>
</div>
<BR>';

?>
