<?php

session_start();

unset($_SESSION["measuresDeclared"]);
unset($_SESSION["measureNames"]);
unset($_SESSION["measureScales"]);

# Reload the initialisation page
header('Location: Measure_Info_Init.php') ;

?>	
