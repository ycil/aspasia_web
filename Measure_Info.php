<?php

# Session start must be in all pages that write / access session variables
session_start();

include("components/interfaceSettings.php");
include("components/menu.php");

print '<HTML><HEAD>';
PrintHeader("ASPASIA: Measure Initialisation");

# Javascript method to check the data entry on the form
print '<script type="text/javascript">
function checkGlobal() 
{
  var measure = document.getElementById("measureName").value;
  var scale = document.getElementById("scale").value;

  if (measure == "") { alert("You must enter an Measure Name"); return false; }
  else 
  { 
 	if (scale == "") { alert("You must specify a scale for this measure"); return false; }
		else
		{
			return true; 

		}

  }
}


function checkNewEvent() 
{
  var param = document.getElementById("parameterName").value;
  var val = document.getElementById("value").value;

  if (param == "") { alert("You must enter an SBML Parameter Name"); return false; }
  else 
  { 
 	if (val == "") { alert("You must specify a value"); return false; }
		else
		{
				return true; 
		}

  }
}
  
</script></head>';


#### Now set up the HTML Page

	if($_SESSION['experimentType']=="Robustness" || $_SESSION['experimentType']=="LHC" || $_SESSION['experimentType']=="eFAST")
	{
		print '<form name="AnalysisSpecificMeasures" action="Measure_Info_Process.php" method="post">
		<br><div id="textheading">Global Sensitivity Analysis using '.$_SESSION['experimentType'].': Output Measure Initialisation</div><br>

		<div id="formtable">
		<table width="85%"  border="0" cellspacing="0" cellpadding="0">
		<TR><TD>A change in parameter value could impact a number of model output responses<br>
		Here, you need to state the SBML model responses you are interested in examining.<br><br>
		Once you have run the generated SBML model files, you can then use ASPASIA to gain a statistical measure of the impact the change in parameter has had on each measure<br>
		Note that no error checking is used to determine whether this parameter exists in the SBML file
		<br><br>
		For a more detailed description of what is required, hover your mouse over the data entry box</TD></TR></TABLE>
		</div>
		<BR>

		<table><TR title="Enter the name of the SBML Measure you are interested in">
		<TD>SBML Measure Name: </TD><TD><input type="text" size="40" id="measureName" name="measureName"></TD></TR>
		<TR title="Enter the units of this measure (e.g. quantity, hours, etc)">
		<TD>Unit of this Measure </TD> <TD><input type="text" size="20" name="scale" id="scale"></TD></TR></TABLE>

		<HR>
		<input type="submit" name="addAnother" value="Add Another Measure" onclick="return checkGlobal()"><input type="submit" value="Next Step: Analysis Specific Parameters">
</form>';

	}
	else if($_SESSION['experimentType']=="NewEvent")
	{
		print '<form name="AnalysisSpecificParams" action="Parameter_Info_Process.php" method="post">
		<br><div id="textheading">Simulate an Intervention: Initialisation</div><br>

		<div id="formtable">
		<table width="85%"  border="0" cellspacing="0" cellpadding="0">
		<TR><TD>Using this technique, you can introduce an intervention once an SBML Model has reached a steady state<br><br>
		This intervention may be by changing the value of a parameter, or the initial concentration of a species.<br>
		ASPASIA does this by reading in the output from an SBML solver. The final line of that results file is then set as the initial state of an SBML model. The intervention should change aspects of that results line, and is specified here.<br><br>
		Note that no error checking is used to determine whether this parameter exists in the SBML file
		<br><br>
		For a more detailed description of what is required, hover your mouse over the data entry box</TD></TR></TABLE>
		</div>
		<BR>

		<table><TR title="Enter the name, as it appears in the SBML file, of the parameter or species whose value or initial concentration you want to change after the steady state">
		<TD>SBML Parameter / Species Name: </TD><TD><input type="text" size="40" id="parameterName" name="parameterName"></TD></TR>
		<TR title="Enter the value to set this parameter or initial species to">
		<TD> Value To Set this Parameter / Initial Concentration </TD> <TD><input type="text" size="5" id="value" name="value"></TD></TR>
		</TABLE>
		<HR>
		<input type="submit" name="addAnother" value="Add Another Parameter" onclick="return checkNewEvent()"><input type="submit" 			value="Next Step: Analysis Specific Parameters">
		</form>';
	}

# See if any parameters have been declared as yet - if so we need to display these details
if(count($_SESSION["measureNames"])>0)
{
	print '<br><div id="textheading">Measures Previously Declared:</div><br>
	<TABLE cellpadding=5>';

	if($_SESSION['experimentType']=="Robustness" || $_SESSION['experimentType']=="LHC" || $_SESSION['experimentType']=="eFAST")
	{
		print '<TD align="center"><b>Measure Name</b></TD>';
		print '<TD align="center"><b>Measure Unit</b></TD>';
	}
	else if($_SESSION['experimentType']=="NewEvent")
	{
		print '<TD align="center"><b>Parameter Name</b></TD>';
		print '<TD align="center"><b>Value</b></TD>';
	}

	$p=0;
	while($p<count($_SESSION['measureNames']))
	{
		print '<TR><TD align="center">'.$_SESSION["measureNames"][$p].'</TD>';

		if($_SESSION['experimentType']=="Robustness" || $_SESSION['experimentType']=="LHC" || $_SESSION['experimentType']=="eFAST")
		{
			print '<TD align="center"><b>'.$_SESSION["measureScales"][$p].'</b></TD>';
		}
		else if($_SESSION['experimentType']=="NewEvent")
		{

		}

		$p=$p+1;
	}

		# Reset Button
		print '</table><form method="post" action="Measure_Info_Reset.php">
		<INPUT type="submit" value="Reset All Measures" name="Reset">
		</form>';



}



?>
