<?php

# Import the layout and menu
include("components/interfaceSettings.php");
include("components/menu.php");

PrintHeader("ASPASIA: Publication");

# Javascript method to check the data entry on the form
print '</head>';


#### Now set up the HTML Page

print '
<br>
<div id="textheading">ASPASIA: Publication</div>
<br>
<div id="formtable">
<table width="85%"  border="0" cellspacing="0" cellpadding="0">
<TR><TD>A software article describing the development and application of ASPASIA is currently in review. This paper will be made available from this website when the manuscript is published.</TD></TR></TABLE>
</div>
<BR>';

?>
