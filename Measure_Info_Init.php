<?php

# Session start must be in all pages that write / access session variables
session_start();

# Set up the arrays for the parameter information

if(!isset($_SESSION["measuresDeclared"]))
{
	# this parameter monitors this. This is also unset if the user resets the arrays
	$_SESSION["measuresDeclared"]="True";

	# Now an array to hold the details of each parameter
	$_SESSION["measureNames"] = array();
	$_SESSION["measureScales"] = array();
}

# Now relocate to the screen to add worlds to the domain
header('Location: Measure_Info.php') ;	






?>
